# ESignature

ESignature est un logiciel de signature numérique de stagiaires et d'intervenants lors de cursus de formation.

Avec cet outil, l'intervenant a accès aux différents modules proposés par le centre de formation et peut renseigner les élèves présents ou non grâce à un système de signature.

L'élève peut signer via :

- un mail de signature envoyé par l'intervenant
- un QRCode généré
- directement sur la tablette de l'intervenant

## Pré-requis

- Angular v13.0.0
- NodeJS v16.15.1
- npm 8.0.2

## Installation

1) Cloner le projet
2) A la racine du projet, installer les dépendances ```npm i ```
3) Lancer le projet ```npm run```

NB : Le mode PWA est utilisable seulement en mode production et non en développement. Pour le tester, vous pouvez installer ```http-server``` et executer la commande ```npm run start-pwa```.

1) ```ng build --prod```  
2) ```http-server ./dist/e-signature -o```

## Spécification techniques

*cf document fourni avec le rendu*