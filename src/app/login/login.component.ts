import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    login: new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$")]),
    password: new FormControl('', Validators.required)
  });

  displayForgotPwdAlert: boolean = false;

  constructor(private authService: AuthService, private router: Router, private msgService: MessageService) { }

  ngOnInit(): void {
  }

  login(): void {
    this.loginForm.updateValueAndValidity();
    const login = this.loginForm.controls['login'].value;
    const password = this.loginForm.controls['password'].value;
    if (this.loginForm.valid) {
      this.authService.login(login, password).then((response) => {
        if (response.status) {
          this.authService.setSession(response);
          setTimeout(() => {
            this.router.navigate(['/dashboard']);
          }, 2000);
        } else {
          this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Veuillez vérifier vos identifiants' });
        }
      });
    } else {
      this.msgService.add({ severity: 'warn', summary: 'Attention', detail: 'Veuillez saisir un login et un mot de passe correct' });
    }
  }

  get email() {
    return this.loginForm.get('login')
  }

  get password() {
    return this.loginForm.get('password')
  }

  showDialog() {
    this.displayForgotPwdAlert = true;
  }

}
