import { Component, OnInit, ViewChild } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad';
import { MessageService } from 'primeng/api';
import { Module, SignatureState, Student } from '../_models/signature';
import { ModulesService } from '../_services/modules.service';

export interface StudentStatus {
  waiting: Student[];
  signed: Student[];
  absent: Student[];
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  modules: Module[] = [];
  loading: boolean = false;
  students: StudentStatus = {
    absent: [],
    signed: [],
    waiting: []
  };
  username: any = '';
  activesModules: Module[] = [];
  closedModule!: Module;
  isOnline: boolean = true;

  drawing: boolean = false;
  @ViewChild(SignaturePad) signaturePad?: SignaturePad;
  signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 200,
    'canvasWidth': 500,
    'canvasHeight': 200
  };

  constructor(private moduleService: ModulesService, private msgService: MessageService) { }

  ngOnInit(): void {
    this.loadModules();
    this.username = localStorage.getItem('username') !== null ? localStorage.getItem('username') : '';
  }

  loadModules(): void {
    this.moduleService.online$().subscribe((status) => {
      this.isOnline = status;
      if (!status) {
        this.moduleService.getOfflineData('modules').subscribe((response) => {
          this.loading = false;
          if (response.status) {
            this.modules = response.data;
            this.getStudentsStatus();
            this.activesModules = this.modules.filter((module) => module.status === 1);
          } else {
            this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Veuillez vous connecter à Internet pour enregistrer les données en amont' });
          }
        });
      }
    });
    this.moduleService.getAllModules().then((response) => {
      this.modules = response.modules;
      this.getStudentsStatus();
      this.activesModules = this.modules.filter((module) => module.status === 1);
    });
  }

  getStudentsStatus(): void {
    this.modules.forEach((module) => {
      module.waiting = module.students.filter((student) => student.status === SignatureState.expected);
      module.absent = module.students.filter((student) => student.status === SignatureState.absent);
      module.present = module.students.filter((student) => student.status === SignatureState.signed);
      if (module.status === 1) {
        module.students.forEach((student) => {
          switch (student.status) {
            case SignatureState.expected:
              this.students.waiting.push(student);
              break;
            case SignatureState.signed:
              this.students.signed.push(student);
              break;
            default:
              this.students.absent.push(student);
              break;
          }
        });
      }
    });
  }

  resend(): void {
    const students = [...this.students.absent, ...this.students.waiting];
    this.moduleService.sendEmails(students).subscribe((response) => {
      if (response.status) {
        this.msgService.add({ severity: 'success', summary: 'Relance envoyée', detail: 'Un mail de relance a bien été envoyé aux élèves en attente' });
      } else {
        this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de l\'envoi du mail, veuillez réessayer' });
      }
    }, (error) => {
      this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de l\'envoi du mail, veuillez réessayer' });
    });
  }

  closeModule(module: Module): void {
    const students = module.students.filter((student) => student.status === SignatureState.expected);
    if (students.length > 0) {
      this.msgService.add({ severity: 'warn', summary: 'Attention', detail: 'Un des élèves est en attente, vous ne pouvez pas fermer la session' });
    } else {
      this.closedModule = module;
    }
  }


  resize(): void {
    this.signaturePad?.clear();
  }

  close(moduleId: string): void {
    if (this.signaturePad) {
      const signature: string = this.signaturePad.toDataURL().toString();
      this.moduleService.closeModule(moduleId, signature).subscribe((response) => {
        if (response.status) {
          this.msgService.add({ severity: 'success', summary: 'Signature envoyée', detail: 'Le module a bien été fermé' });
          this.loadModules();
        } else {
          this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de la clotûre de la session, veuillez réessayer' });
        }
      }, (error) => {
        this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de la clotûre de la session, veuillez réessayer' });
      });
    }
  }


}
