import { Component, OnDestroy, OnInit, Output } from '@angular/core';
import { MenuItem, Message, MessageService } from 'primeng/api';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../_services/auth.service';
import { ModulesService } from '../_services/modules.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit, OnDestroy {

  items!: MenuItem[];
  subscription!: Subscription;
  isLoggedIn: boolean = false;
  msgs!: Message[];
  onlineAlreadyDefined: boolean = false;

  onlineSubscription!: ReturnType<typeof setInterval>;

  constructor(public authService: AuthService, private moduleService: ModulesService, private msgService: MessageService) { }

  ngOnInit(): void {
    this.isOnline();
    this.subscription = this.authService.loggedIn.subscribe((state) => {
      if (state === false) {
        clearInterval(this.onlineSubscription);
      }
      this.isLoggedIn = state;
      this.items = [
        {
          label: 'Accueil',
          icon: 'pi pi-fw pi-home',
          routerLink: '/dashboard',
          visible: state
        },
        {
          label: 'Modules',
          icon: 'pi pi-fw pi-box',
          routerLink: '/modules',
          visible: state
        }
      ];
    });

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    clearInterval(this.onlineSubscription);
  }

  logout(): void {
    this.authService.logout();
    clearInterval(this.onlineSubscription);
  }

  sync(): void {
    this.moduleService.syncData();
  }

  showViaService() {
    this.msgService.add({ severity: 'success', summary: 'Service Message', detail: 'Via MessageService' });
  }

  noConnectionWarning() {
    if (!this.onlineAlreadyDefined && this.isLoggedIn) {
      this.msgs = [{ severity: 'warn', summary: 'Pas de connexion', detail: 'Vous n\'êtes plus connecté à Internet, les données seront mises à jour lorsque la connexion sera de nouveau établie' }];
    }
    this.onlineAlreadyDefined = true;
  }

  hide() {
    this.msgs = [];
  }

  isOnline(): void {
    this.onlineSubscription = setInterval(() => {
      this.moduleService.online$().subscribe((state) => {
        if (state === false) {
          this.noConnectionWarning();
        } else {
          this.onlineAlreadyDefined = false;
        }
      });
    }, 5000);
  }
}
