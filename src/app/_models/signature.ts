export enum SignatureState {
    expected = 'attendu',
    signed = 'signé',
    absent = 'absent'
}

export interface Login {
    login: string;
    password: string;
}

export interface LoginResponse {
    status: boolean;
    message?: string;
    error?: string;
    token: string;
    username: string;
}

export interface ModuleListResponse {
    status: boolean;
    error?: string;
    modules: Module[];
}

export interface Module {
    id: string;
    name: string;
    date: string;
    location: string;
    duration: string;
    starting: string;
    ending: string;
    comment: string;
    status: number;
    description: string;
    teachers: Teacher[];
    students: Student[];
    waiting?: Student[];
    absent?: Student[];
    present?: Student[];
}

export interface Teacher {
    name: string;
    address: string;
    birthday: string;
    email: string;
    login: string;
    password: string;
}

export interface Student {
    id: string;
    name: string;
    email: string;
    address: string;
    birthday: string;
    status: string;
    comment: string;
}
