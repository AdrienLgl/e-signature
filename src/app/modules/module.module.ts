import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleRoutingModule } from './module-routing.module';
import { TableModule } from 'primeng/table';
import { CalendarModule } from 'primeng/calendar';
import { SliderModule } from 'primeng/slider';
import { DialogModule } from 'primeng/dialog';
import { MultiSelectModule } from 'primeng/multiselect';
import { ContextMenuModule } from 'primeng/contextmenu';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { InputTextModule } from 'primeng/inputtext';
import { ProgressBarModule } from 'primeng/progressbar';
import { DropdownModule } from 'primeng/dropdown';
import { ModulesComponent } from './modules.component';
import { ModuleDetailsComponent } from './module-details/module-details.component';
import { PanelModule } from 'primeng/panel';
import { MessageService } from 'primeng/api';
import { SignatureComponent } from './signature/signature.component';
import { SignaturePadModule } from 'angular2-signaturepad';
import { ManualSignatureComponent } from './manual-signature/manual-signature.component';
import { QRCodeModule } from 'angularx-qrcode';
import { QrCodeComponent } from './qr-code/qr-code.component';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ReturnButtonComponent } from '../return-button/return-button.component';
import { CardModule } from 'primeng/card';
import { TagModule } from 'primeng/tag';
import { FullCalendarModule } from '@fullcalendar/angular'; // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction'; // a plugin!
import { ImageModule } from 'primeng/image';

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin
]);

@NgModule({
  declarations: [
    ModulesComponent,
    ModuleDetailsComponent,
    SignatureComponent,
    ManualSignatureComponent,
    QrCodeComponent,
    ReturnButtonComponent
  ],
  imports: [
    ModuleRoutingModule,
    CommonModule,
    TableModule,
    CalendarModule,
    SliderModule,
    DialogModule,
    MultiSelectModule,
    ContextMenuModule,
    DropdownModule,
    ButtonModule,
    ToastModule,
    InputTextModule,
    ProgressBarModule,
    PanelModule,
    SignaturePadModule,
    QRCodeModule,
    InputTextareaModule,
    CardModule,
    TagModule,
    FullCalendarModule,
    ImageModule
  ],
  providers: [
    MessageService
  ]
})
export class ModuleModule { }
