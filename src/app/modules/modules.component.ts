import { Component, OnInit, ViewChild } from '@angular/core';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { Table } from 'primeng/table';
import { ModulesService } from '../_services/modules.service';
import { Location } from '@angular/common';
import { CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking
import { Router } from '@angular/router';
import frLocale from '@fullcalendar/core/locales/fr';
import { Module, SignatureState, Student } from '../_models/signature';
import { SignaturePad } from 'angular2-signaturepad';



export interface StudentStatus {
  waiting: Student[];
  signed: Student[];
  absent: Student[];
}



@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {

  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    events: [],
    locale: frLocale,
    eventClick: (event) => { this.router.navigate(['/modules/' + event.event._def.publicId]) }
  };


  modules: Module[] = [];
  selectedModules: Module[] = [];
  statuses?: any[];
  representatives: any[] = [];
  view: string = "card";
  @ViewChild('dt') table!: Table;


  loading: boolean = false;
  students: StudentStatus = {
    absent: [],
    signed: [],
    waiting: []
  };
  username: any = '';
  activesModules: Module[] = [];
  closedModule!: Module;
  isOnline: boolean = true;

  drawing: boolean = false;
  @ViewChild(SignaturePad) signaturePad?: SignaturePad;
  signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 200,
    'canvasWidth': 500,
    'canvasHeight': 200
  };

  constructor(private moduleService: ModulesService, private primengConfig: PrimeNGConfig, private msgService: MessageService, private router: Router) { }

  ngOnInit(): void {
    this.getAllModules();
    this.primengConfig.ripple = true;
  }

  getAllModules(): void {
    this.moduleService.online$().subscribe((status) => {
      if (!status) {
        this.moduleService.getOfflineData('modules').subscribe((response) => {
          this.loading = false;
          if (response.status) {
            this.modules = response.data;
            this.loadCalendar();
          } else {
            this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Veuillez vous connecter à Internet pour enregistrer les données en amont' });
          }
        });
      }
    });
    this.moduleService.getAllModules().then((response) => {
      this.modules = response.modules;
      this.loadCalendar();
      this.moduleService.saveOfflineData('modules', JSON.stringify(response.modules));
      this.loading = false;
    });
  }

  onActivityChange(event: any) {
    const value = event.target.value;
    if (value && value.trim().length) {
      const activity = parseInt(value);

      if (!isNaN(activity)) {
        this.table?.filter(activity, 'activity', 'gte');
      }
    }
  }

  onDateSelect(value: any) {
    this.table?.filter(this.formatDate(value), 'date', 'equals')
  }

  loadCalendar(): void {
    this.modules.forEach((module) => {
      this.calendarOptions.events = [];
      this.calendarOptions.events.push(
        {
          title: module.name,
          date: module.starting,
          description: module.description,
          start: module.starting,
          end: module.ending,
          id: module.id
        }
      );
    });
  }

  formatDate(date: any) {
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (month < 10) {
      month = '0' + month;
    }

    if (day < 10) {
      day = '0' + day;
    }

    return date.getFullYear() + '-' + month + '-' + day;
  }

  onRepresentativeChange(event: any) {
    this.table?.filter(event.value, 'representative', 'in')
  }

  onSelectCardView() {
    this.view = "card";
    console.log("card");
  }
  onSelectCalendarView() {
    this.view = "calendar";
  }
  onSelectTableView() {
    this.view = "table";
  }

  // closeModule(module: Module): void {
  //   const students = module.students.filter((student) => student.status === SignatureState.expected);
  //   if (students.length > 0) {
  //     this.msgService.add({ severity: 'warn', summary: 'Attention', detail: 'Un des élèves est en attente, vous ne pouvez pas fermer la session' });
  //   } else {
  //     this.closedModule = module;
  //   }
  // }

  // close(moduleId: string): void {
  //   if (this.signaturePad) {
  //     const signature: string = this.signaturePad.toDataURL().toString();
  //     this.moduleService.closeModule(moduleId, signature).subscribe((response) => {
  //       if (response.status) {
  //         this.msgService.add({ severity: 'success', summary: 'Signature envoyée', detail: 'Le module a bien été fermé' });
  //         this.loadModules();
  //       } else {
  //         this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de la clotûre de la session, veuillez réessayer' });
  //       }
  //     }, (error) => {
  //       this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de la clotûre de la session, veuillez réessayer' });
  //     });
  //   }
  // }

  // loadModules(): void {
  //   this.moduleService.online$().subscribe((status) => {
  //     this.isOnline = status;
  //     if (!status) {
  //       this.moduleService.getOfflineData('modules').subscribe((response) => {
  //         this.loading = false;
  //         if (response.status) {
  //           this.modules = response.data;
  //           this.getStudentsStatus();
  //           this.activesModules = this.modules.filter((module) => module.status === 1);
  //         } else {
  //           this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Veuillez vous connecter à Internet pour enregistrer les données en amont' });
  //         }
  //       });
  //     }
  //   });
  //   this.moduleService.getAllModules().then((response) => {
  //     this.modules = response.modules;
  //     this.getStudentsStatus();
  //     this.activesModules = this.modules.filter((module) => module.status === 1);
  //   });
  // }

  // getStudentsStatus(): void {
  //   this.modules.forEach((module) => {
  //     module.waiting = module.students.filter((student) => student.status === SignatureState.expected);
  //     module.absent = module.students.filter((student) => student.status === SignatureState.absent);
  //     module.present = module.students.filter((student) => student.status === SignatureState.signed);
  //     if (module.status === 1) {
  //       module.students.forEach((student) => {
  //         switch (student.status) {
  //           case SignatureState.expected:
  //             this.students.waiting.push(student);
  //             break;
  //           case SignatureState.signed:
  //             this.students.signed.push(student);
  //             break;
  //           default:
  //             this.students.absent.push(student);
  //             break;
  //         }
  //       });
  //     }
  //   });
  // }

}
