import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ModulesService } from 'src/app/_services/modules.service';
import { SignatureState, Student } from 'src/app/_models/signature';
import { ActivatedRoute, Router } from '@angular/router';
import { SignaturePad } from 'angular2-signaturepad';
import { Module } from 'src/app/_models/signature';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-manual-signature',
  templateUrl: './manual-signature.component.html',
  styleUrls: ['./manual-signature.component.css']
})
export class ManualSignatureComponent implements OnInit, AfterViewInit {

  participants: Student[] = [];
  module!: Module;
  moduleId!: string;
  signature!: Student;
  drawing: boolean = false;
  loading: boolean = true;
  signaturesImage: string[] = [];
  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 200,
    'canvasWidth': 500,
    'canvasHeight': 200
  };

  constructor(private moduleService: ModulesService, private activatedRoute: ActivatedRoute, private msgService: MessageService, private router: Router) {
    const snapshot = this.activatedRoute.snapshot;
    this.moduleId = snapshot.params['moduleId'];
  }

  ngOnInit(): void {
    this.getManuallySignatures();
    this.getModule(this.moduleId);
  }

  getModule(moduleId: string): void {
    this.loading = true;
    this.moduleService.getModule(moduleId).then((response) => {
      if (response) {
        this.module = response;
      } else {
        this.msgService.add({ severity: 'error', summary: 'Error', detail: 'Error during module loading, please check your connection' });
        this.loading = false;
        setTimeout(() => {
          this.router.navigate(['/modules']);
        }, 2000);
      }

    }, (error) => {
      console.error(error);
      this.msgService.add({ severity: 'error', summary: 'Error', detail: 'Error during module loading, please check your connection' });
      this.loading = false;
      setTimeout(() => {
        this.router.navigate(['/modules']);
      }, 2000);
    });
  }

  ngAfterViewInit(): void {
    this.signaturePad?.set('minWidth', 5); // set szimek/signature_pad options at runtime
    this.signaturePad?.set('border', '1px solid black');
    this.signaturePad?.clear();
  }

  getManuallySignatures(): void {
    this.moduleService.getManuallySignature(this.moduleId).subscribe((response) => {
      this.participants = response;
    });
  }

  openSignature(student: Student): void {
    console.log(student);
    this.signature = student;
  }

  closeSignature(student: Student): void {
    if (this.signaturePad?.toDataURL()) {
      this.participants.map((st) => {
        if (st.id === student.id) {
          st.status = SignatureState.signed;
        }
      })
    }
  }

  beginToDraw(): void {
    this.drawing = true;
  }

  drawComplete(): void {
    this.signaturesImage.push(this.signaturePad?.toDataURL());
  }

  resize(): void {
    this.signaturePad?.clear();
  }
}
