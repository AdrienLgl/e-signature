import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualSignatureComponent } from './manual-signature.component';

describe('ManualSignatureComponent', () => {
  let component: ManualSignatureComponent;
  let fixture: ComponentFixture<ManualSignatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManualSignatureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualSignatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
