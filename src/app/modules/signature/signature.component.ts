import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SignaturePad } from 'angular2-signaturepad';
import { MessageService } from 'primeng/api';
import { Student } from 'src/app/_models/signature';
import { ModulesService } from 'src/app/_services/modules.service';

@Component({
  selector: 'app-signature',
  templateUrl: './signature.component.html',
  styleUrls: ['./signature.component.css']
})
export class SignatureComponent implements OnInit, AfterViewInit {

  signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 500,
    'canvasWidth': 1000,
    'canvasHeight': 450,
    'border': '2px solid black'
  };

  signature?: string;
  studentId!: string;
  moduleId!: string;
  student!: Student;
  drawing: boolean = false;
  @ViewChild(SignaturePad) signaturePad?: SignaturePad;


  constructor(private moduleService: ModulesService, private activatedRoute: ActivatedRoute, private msgService: MessageService, private router: Router) {
    const snapshot = this.activatedRoute.snapshot;
    this.studentId = snapshot.params['studentId'];
    this.moduleId = snapshot.params['moduleId'];
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.signaturePad?.set('minWidth', 5); // set szimek/signature_pad options at runtime
    this.signaturePad?.set('border', '1px solid black');
    this.signaturePad?.clear();
  }

  getStudent(): void {
    this.moduleService.getStudent(this.moduleId, this.studentId).then((response) => {
      this.student = response;
    });
  }

  saveSignature(): void {
    this.signature = this.signaturePad?.toDataURL();
    this.moduleService.postSignature(this.moduleId, this.studentId).subscribe((response) => {
      if (response.status === true) {
        this.msgService.add({ severity: 'success', summary: 'Signature envoyée', detail: 'Votre signature a bien été transmise' });
        setTimeout(() => {
          this.router.navigate([`/modules/${this.moduleId}`]);
        }, 2000);
      }
    });
  }

  beginToDraw(): void {
    this.drawing = true;
  }

  resize(): void {
    this.signaturePad?.clear();
  }

}
