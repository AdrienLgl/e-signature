import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../_helpers/auth.guard';
import { ModuleDetailsComponent } from './module-details/module-details.component';
import { QrCodeComponent } from './qr-code/qr-code.component';
import { SignatureComponent } from './signature/signature.component';
import { ModulesComponent } from './modules.component';

const routes: Routes = [
  { path: 'signature/:moduleId/:studentId', component: SignatureComponent },
  { path: 'qrcode/:moduleId', component: QrCodeComponent, canActivate: [AuthGuard] },
  { path: ':moduleId', component: ModuleDetailsComponent, canActivate: [AuthGuard] },
  { path: '', component: ModulesComponent, canActivate: [AuthGuard] }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class ModuleRoutingModule { }
