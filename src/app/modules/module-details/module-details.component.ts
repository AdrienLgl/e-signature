import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SignaturePad } from 'angular2-signaturepad';
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { Module, SignatureState, Student } from 'src/app/_models/signature';
import { ModulesService } from 'src/app/_services/modules.service';

@Component({
  selector: 'app-module-details',
  templateUrl: './module-details.component.html',
  styleUrls: ['./module-details.component.css']
})
export class ModuleDetailsComponent implements OnInit, OnDestroy, AfterViewInit {

  module!: Module;
  moduleId!: string;
  students: Student[] = [];
  selectedStudents: Student[] = [];
  statuses?: any[];
  representatives: any[] = [];
  loading: boolean = true;
  @ViewChild('dt') table!: Table;

  waitingStudents: Student[] = [];
  presentStudents: Student[] = [];
  absentStudents: Student[] = [];

  onlineStatus: boolean = true;

  teacherSignature!: string;
  student!: any;
  drawing: boolean = false;
  @ViewChild(SignaturePad) signaturePad?: SignaturePad;
  signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 200,
    'canvasWidth': 500,
    'canvasHeight': 200
  };

  constructor(private moduleService: ModulesService, private activatedRoute: ActivatedRoute, private msgService: MessageService, private router: Router) {
    const snasphot = this.activatedRoute.snapshot;
    this.moduleId = snasphot.params['moduleId'];
  }

  ngOnInit(): void {
    this.getModule(this.moduleId);
  }

  ngOnDestroy(): void {
    this.msgService.clear();
  }


  ngAfterViewInit(): void {
    this.signaturePad?.set('minWidth', 5); // set szimek/signature_pad options at runtime
    this.signaturePad?.set('border', '1px solid black');
    this.signaturePad?.clear();
  }

  getModule(moduleId: string): void {
    this.loading = true;
    this.moduleService.online$().subscribe((status) => {
      this.onlineStatus = status;
      if (!status) {
        this.moduleService.getOfflineData('modules').subscribe((response) => {
          this.loading = false;
          if (response.status) {
            this.module = response.data.filter((m: Module) => m.id === moduleId)[0];
          } else {
            this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Veuillez vous connecter à Internet pour enregistrer les données en amont' });
          }
        });
      }
    });
    this.moduleService.getModule(moduleId).then((response) => {
      if (response) {
        this.module = response;
        this.students = response.students;
        this.loading = false;
        this.filterStudents();
      } else {
        this.msgService.add({ severity: 'error', summary: 'Error', detail: 'Error during module loading, please check your connection' });
        this.loading = false;
        setTimeout(() => {
          this.router.navigate(['/modules']);
        }, 2000);
      }
    }, (error) => {
      console.error(error);
      this.msgService.add({ severity: 'error', summary: 'Error', detail: 'Error during module loading, please check your connection' });
      this.loading = false;
      setTimeout(() => {
        this.router.navigate(['/modules']);
      }, 2000);
    });
  }

  filterStudents(): void {
    this.presentStudents = [];
    this.absentStudents = [];
    this.waitingStudents = [];
    this.students.forEach((student) => {
      switch (student.status) {
        case SignatureState.signed:
          this.presentStudents.push(student);
          break;
        case SignatureState.expected:
          this.waitingStudents.push(student);
          break;
        default:
          this.absentStudents.push(student);
          break;
      }
    });
  }


  absent(studentId: string): void {
    this.students.forEach((student) => {
      if (student.id === studentId) {
        student.status = SignatureState.absent;
      }
    });
    this.filterStudents();
  }

  manuallySigned(): void {
    this.moduleService.manuallySigned(this.selectedStudents, this.moduleId).subscribe((response) => {
      if (response.status) {
        this.router.navigate(['/emargement_manuel/' + this.moduleId]);
      }
    }, (error) => {
      this.msgService.add({ severity: 'error', summary: 'Error', detail: 'Error during listing students, please retry' });
    })
  }

  sendEmail(): void {
    this.moduleService.sendEmails(this.selectedStudents).subscribe((response) => {
      if (response.status) {
        this.msgService.add({ severity: 'success', summary: 'Relance envoyée', detail: 'Un mail de signature a bien été envoyé aux élèves en attente' });
      } else {
        this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de l\'envoi du mail, veuillez réessayer' });
      }
    }, (error) => {
      this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de l\'envoi du mail, veuillez réessayer' });
    });
  }

  generateQrCode(): void {
    this.moduleService.manuallySigned(this.selectedStudents, this.moduleId).subscribe((response) => {
      if (response.status) {
        this.router.navigate(['/modules/qrcode/' + this.moduleId]);
      }
    }, (error) => {
      this.msgService.add({ severity: 'error', summary: 'Error', detail: 'Error during listing students, please retry' });
    })
  }

  closeModule(): void {
    this.filterStudents();
    if (this.waitingStudents.length > 0) {
      this.msgService.add({ severity: 'warn', summary: 'Attention', detail: 'Un des élèves est en attente, vous ne pouvez pas fermer la session' });
    } else {
      this.teacherSignature = 'true';
    }
  }

  close(): void {
    if (this.signaturePad) {
      const signature: string = this.signaturePad.toDataURL().toString();
      this.moduleService.closeModule(this.moduleId, signature).subscribe((response) => {
        if (response.status) {
          this.msgService.add({ severity: 'success', summary: 'Signature envoyée', detail: 'Le module a bien été fermé' });
          setTimeout(() => {
            this.router.navigate(['/modules']);
          }, 2000);
        } else {
          this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de la clotûre de la session, veuillez réessayer' });
        }
      }, (error) => {
        this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de la clotûre de la session, veuillez réessayer' });
      });
    }
  }

  beginToDraw(): void {
    this.drawing = true;
  }

  resize(): void {
    this.signaturePad?.clear();
  }

  resend(): void {
    const students = this.students.filter((student) => student.status !== SignatureState.signed);
    this.moduleService.online$().subscribe((state) => {
      if (state) {
        this.moduleService.sendEmails(students).subscribe((response) => {
          if (response.status) {
            this.msgService.add({ severity: 'success', summary: 'Relance envoyée', detail: 'Un mail de relance a bien été envoyé aux élèves en attente' });
          } else {
            this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de l\'envoi du mail, veuillez réessayer' });
          }
        }, (error) => {
          this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de l\'envoi du mail, veuillez réessayer' });
        });
      } else {
        this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Veuillez vous connecter à Internet' });
      }
    })

  }

  openStudentComment(student: Student): void {
    this.student = student;
  }

  saveStudentComment(studentId: string, comment: string): void {
    this.student = undefined;
    if (comment !== '') {
      this.students.map((student) => { if (student.id === studentId) { student.comment = comment } });
      this.moduleService.postStudentComment(this.moduleId, studentId, comment).subscribe((response) => {
        if (response.status) {
          this.msgService.add({ severity: 'success', summary: 'Commentaire sauvegardé', detail: 'Le commentaire a bien été pris en compte' });
        } else {
          this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de la sauvegarde du commentaire, veuillez réessayer' });
        }
      }, (error) => {
        this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Erreur lors de la sauvegarde du commentaire, veuillez réessayer' });
      });
    } else {
      this.msgService.add({ severity: 'warn', summary: 'Attention', detail: 'Veuillez saisir une valeur correcte' });
    }
  }
}
