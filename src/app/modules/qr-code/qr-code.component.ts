import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-qr-code',
  templateUrl: './qr-code.component.html',
  styleUrls: ['./qr-code.component.css']
})
export class QrCodeComponent implements OnInit {

  url!: string;
  moduleId!: string;

  constructor(private activatedRoute: ActivatedRoute) {
    const snapshot = this.activatedRoute.snapshot;
    this.moduleId = snapshot.params['moduleId'];
    this.url = `${window.location.origin}/emargement_manuel/${this.moduleId}`;
  }

  ngOnInit(): void {
  }

}
