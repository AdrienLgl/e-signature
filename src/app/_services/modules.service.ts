import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { fromEvent, map, merge, Observable, Observer, of } from 'rxjs';
import { Module, ModuleListResponse, Student } from '../_models/signature';

@Injectable({
  providedIn: 'root'
})
export class ModulesService {

  constructor(private http: HttpClient, private msgService: MessageService) { }

  getAllModules(): Promise<ModuleListResponse> {
    return this.http.get<any>('assets/_mock_data/data.json')
      .toPromise()
      .then(res => <ModuleListResponse>res['listModulesResponse'])
      .then(data => { return data; });
  }

  getModule(moduleId: string): Promise<Module> {
    return this.http.get<any>('assets/_mock_data/data.json')
      .toPromise()
      .then(res => <ModuleListResponse>res['listModulesResponse'])
      .then(data => {
        data.modules.map((module) => {
          module.ending = new Date(module.ending).toString();
          module.starting = new Date(module.starting).toString();
        });
        return data.modules.filter((module) => module.id === moduleId)[0];
      });
  }

  getStudent(moduleId: string, studentId: string): Promise<Student> {
    return this.http.get<any>('assets/_mock_data/data.json')
      .toPromise()
      .then(res => <ModuleListResponse>res['listModulesResponse'])
      .then(data => {
        return data.modules.filter((module) => module.id === moduleId)[0].students.filter((student) => student.id === studentId)[0];
      });
  }

  updateModule(module: Module): Observable<any> {
    return of({ status: true });
  }

  manuallySigned(students: Student[], moduleId: string): Observable<any> {
    // post students list
    localStorage.setItem(`present_students_${moduleId}`, JSON.stringify(students));
    return of({ status: true });
  }

  getManuallySignature(moduleId: string): Observable<Student[]> {
    const students = localStorage.getItem(`present_students_${moduleId}`);
    return students !== null ? of(JSON.parse(students)) : of([]);
  }

  postSignature(moduleId: string, studentId: string): Observable<any> {
    // post request
    return of({ status: true });
  }

  closeModule(moduleId: string, signature: string, comment?: string): Observable<any> {
    // post module id & teacher signature
    return of({ status: true });
  }

  sendEmails(students: Student[]): Observable<any> {
    // send email
    return of({ status: true });
  }

  postStudentComment(moduleId: string, studentId: string, comment: string): Observable<any> {
    // post on module id
    return of({ status: true });
  }

  saveOfflineData(index: string, data: string): void {
    localStorage.setItem(index, data);
  }

  cleanOfflineData(index: string): void {
    localStorage.removeItem(index);
  }

  getOfflineData(index: string): Observable<any> {
    const data = localStorage.getItem(index);
    if (data !== null) {
      return of({ status: true, data: JSON.parse(data) });
    } else {
      return of({ status: false, data: {} });
    }
  }

  online$() {
    return merge<any>(
      fromEvent(window, 'offline').pipe(map(() => false)),
      fromEvent(window, 'online').pipe(map(() => true)),
      new Observable((sub: Observer<boolean>) => {
        sub.next(navigator.onLine);
        sub.complete();
      }));
  }

  syncData(): void {
    this.online$().subscribe((status) => {
      if (status) {
        // save data to API
        this.getOfflineData('modules').subscribe((response) => {
          if (response.status) {
            const modules: Module[] = response.data;
            let count = 0;
            modules.forEach((module) => {
              count++;
              this.updateModule(module).subscribe((res) => {
                if (res.status) {
                  console.log('Module updated succesfully');
                  if (count === modules.length) {
                    this.msgService.add({ severity: 'success', summary: 'Mise à jour', detail: 'Les données ont bien été mises à jour' });
                  }
                }
              });
            });
          } else {
            this.msgService.add({ severity: 'warn', summary: 'Aucune donnée', detail: 'Vous n\'avez pas perdu votre connexion, aucune donnée n\'a été sauvegardée localement' });
          }
        });
      } else {
        this.msgService.add({ severity: 'error', summary: 'Erreur', detail: 'Veuillez vous connecter à Internet' });
      }
    });
  }

}
