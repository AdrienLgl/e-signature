import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginResponse } from '../_models/signature';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedIn: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: HttpClient, private router: Router) { }

  login(login: string, password: string): Promise<LoginResponse> {
    // check login validity
    return this.http.get<any>('assets/_mock_data/data.json')
      .toPromise()
      .then(res => <LoginResponse>res['loginResponse'])
      .then(data => { return data; });
  }

  isLoggedIn(): boolean {
    const expiration = localStorage.getItem('expiration');
    if (expiration === null) {
      this.loggedIn.emit(false);
      return false;
    }
    const now = new Date().getTime();
    if (parseInt(expiration) < now) {
      this.loggedIn.emit(false);
      return false;
    }
    this.loggedIn.emit(true);
    return true;
  }

  setSession(auth: LoginResponse): void {
    localStorage.setItem('username', auth.username);
    localStorage.setItem('id_token', auth.token);
    const date = new Date();
    const now = new Date(date);
    now.setMinutes(date.getMinutes() + 20);
    localStorage.setItem('expiration', now.getTime().toString());
  }

  logout(): void {
    localStorage.removeItem('username');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expiration');
    setTimeout(() => {
      this.loggedIn.emit(false);
      this.router.navigate(['/login']);
    }, 2000);
  }


}
