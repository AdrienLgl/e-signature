import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-return-button',
  templateUrl: './return-button.component.html'
})
export class ReturnButtonComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit(): void {
  }

  onSelectBack() {
    this.location.back();
  }
}
